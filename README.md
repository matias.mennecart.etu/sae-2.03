# SAÉ 2.03 - Installation de services réseaux

Contributeurs:

- DEKEISER Matisse (matisse.dekeiser.etu@univ-lille.fr)
- MENNECART Matias (matias.mennecart.etu@univ-lille.fr)

### Présentation de la SAÉ:

L'objectif de cette SAÉ était l'installation d'un service réseau (Gitea) et la prise en main de l'outil Markdown. Il fallait tout d'abord installer une machine virtuelle Debian Linux avec l'aide de l'outil VirtualBox; nous avions vu l'installation manuelle au semestre 1, mais ici il s'agit d'une installation automatisée, à l'aide d'un script avec des options systèmes modifiables selon les besoins de l'utilisateur (interface graphique, compte utilisateur, permissions administrateur, installation de paquets supplémentaires, etc.). Ensuite, nous avons installé et vu plusieurs services tels que GitHub ou GitLab, avant d'installer notre propre service en réseau, Gitea. Nous avons dû réaliser un rapport de tout ce qui a été effectué en utilisant l'outil Markdown, un outil de balisage léger permettant la rédaction et la mise en page de documents qui peuvent ensuite être exportés en différents formats (HTML, PDF, …) grâce à l'outil Pandoc.

### Conversion en PDF du rapport:

`pandoc rendu.md -o rendu.pdf --from markdown --template ./resources/eisvogel.latex --listings --number-sections`

### Conversion en HTML du rapport:

`pandoc -s --toc -c resources/style.css rendu.md --from markdown -o rendu.html`
